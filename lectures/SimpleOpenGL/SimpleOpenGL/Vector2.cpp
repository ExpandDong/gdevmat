#include "Vector2.h"



Vector2::Vector2(float x, float y)
{
	mX = x;
	mY = y;
}

Vector2::Vector2()
{
}


Vector2::~Vector2()
{
}

float Vector2::getX()
{
	return mX;
}

float Vector2::getY()
{
	return mY;
}

void Vector2::setX(float x)
{
	mX = x;
}

void Vector2::setY(float y)
{
	mY = y;
}

Vector2 Vector2::operator+(Vector2 vector)
{
	float x, y;
	x = mX + vector.getX();
	y = mY + vector.getY();
	return Vector2(x,y);
}

Vector2 Vector2::operator-(Vector2 vector)
{
	float x, y;
	x = mX - vector.getX();
	y = mY - vector.getY();
	return Vector2(x, y);
}

Vector2 Vector2::operator*(Vector2 vector)
{
	float x, y;
	x = mX * vector.getX();
	y = mY * vector.getY();
	return Vector2(x, y);
}

Vector2 Vector2::operator/(Vector2 vector)
{
	float x, y;
	x = mX / vector.getX();
	y = mY / vector.getY();
	return Vector2(x, y);
}



float Vector2::Magnitude()
{
	float magnitude = sqrt((mX * mX) + (mY * mY));
	return magnitude;
}
