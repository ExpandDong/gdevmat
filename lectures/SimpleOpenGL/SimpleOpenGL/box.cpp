#include "box.h"
#include "Vector2.h"
#include <vector>

#include <GL\freeglut.h>

using namespace std;

Box::Box(float x, float y)
{
	width = 75;
	height = 75;
	this->x = x;
	this->y = y;
}


Box::~Box()
{
}

void Box::draw() {
	glMatrixMode(GL_MODELVIEW);
	glColor3f(1.0, 1.0f, 1.0f);

	/*SECOND METHOD: use matrices*/
		
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glTranslatef(x, y, 0);
	glScalef(width / 2, height / 2, 1.0f);

	glBegin(GL_TRIANGLE_FAN);

	vector<Vector2> vertices;
	int sides = 36;

	vertices.push_back(Vector2(0, 0));
	vertices.push_back(Vector2(0, 1.0f));
	Vector2 temp = vertices[1];

	float rotation = 360 / sides;
	float converter = 3.14 / 180;
	rotation *= converter;

	for (int i = 1; i < sides; i++)
	{
		Vector2 zRotation = temp;
		zRotation.setX(((temp.getX() * (cos(rotation))) + (temp.getY() * (sin(rotation)))));
		zRotation.setY(((temp.getX() * -(sin(rotation))) + (temp.getY() * (cos(rotation)))));
		temp = zRotation;
		vertices.push_back(zRotation);
	}

	for (int i = 1; i < vertices.size(); i++)
	{
		glVertex2f(vertices[vertices.size() - i].getX(), vertices[vertices.size() - i].getY());
	}

	glEnd();

	glPopMatrix();

	// END
}

float Box::getX()
{
	return x;
}

float Box::getY()
{
	return y;
}

void Box::setPosition(float x, float y)
{
	this->x = x;
	this->y = y;
}

bool Box::isInTheBox(float px, float py)
{
	float pleft = px - (width / 2.0f);
	float pright = px + (width / 2.0f);
	float pbottom = py - (height / 2.0f);
	float ptop = py + (height / 2.0f);

	float left = x - (width / 2.0f);
	float right = x + (width / 2.0f);
	float bottom = y - (height / 2.0f);
	float top = y + (height / 2.0f);

	if (pright < left || pleft > right) return false;
	if (ptop < bottom || pbottom > top) return false;

	return true;
}
