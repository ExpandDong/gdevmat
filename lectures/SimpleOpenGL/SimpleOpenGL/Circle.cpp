#include "Circle.h"


Circle::Circle(Vector2 center, float radius)
{
	mX = center.getX();
	mY = center.getY();
	mRadius = radius;
}

Circle::~Circle()
{
}

float Circle::getX()
{
	return mX;
}

float Circle::getY()
{
	return mY;
}

float Circle::getRadius()
{
	return mRadius;
}

float Circle::getDistanceFrom(Circle circle)
{
	float distance = sqrt((((circle.getX() - mX) * (circle.getX() - mX)) + ((circle.getY() - mY) * (circle.getY() - mY))));
	return distance;

}

void Circle::setPosition(float x, float y)
{
	mX = x;
	mY = y;
	cout << "New Position: (" << mX << ", " << mY << ")" << endl;
}
