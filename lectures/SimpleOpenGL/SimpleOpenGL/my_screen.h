#pragma once
class MyScreen
{
public:
	MyScreen();
	~MyScreen();

	void initialize(int w, int h);
	void resize(int w, int h);
	void draw();
};

