#include "my_screen.h"


#include <GL/glut.h>


MyScreen::MyScreen()
{
}


MyScreen::~MyScreen()
{
}

void MyScreen::initialize(int w, int h) {
	glutInitDisplayMode(0);
	glutInitWindowSize(w, h);
	glutCreateWindow("Bas Prog 3");

}

void MyScreen::resize(int w, int h) {
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-w / 2.0f, w / 2.0f, -h / 2.0f, h / 2.0f);
}

void MyScreen::draw() {
	/*for(int i = 0; i < shapeCount; i++)	
		shapes[i]->draw();*/
}