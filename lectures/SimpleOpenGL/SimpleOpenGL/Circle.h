#pragma once
#include <iostream>
#include "Vector2.h"

using namespace std;

class Circle
{
public:
	Circle(Vector2 center, float radius);
	~Circle();

	float getX();
	float getY();
	float getRadius();

	float getDistanceFrom(Circle circle);
	void setPosition(float x, float y);

private:
	float mX;
	float mY;
	float mRadius;
};

