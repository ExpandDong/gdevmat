#pragma once
#include "box.h"

class Game
{
public:
	Game();
	~Game();

	void initialize();
	void resize(float w, float h);
	void draw();
	void update(float timeDelta);
	void keyPressed(unsigned char key);

private:
	Box *box;
	Box *box2;
	Box *box3;
	Box *box4;
	Box *box5;
};

