

#include <gl/glut.h>
#include "my_screen.h"

#include "game.h"

#include <iostream>
#include <stdlib.h>
#include <stdio.h>

#include <ctime>
using std::cout;
using std::clock;

Game *game;
clock_t last_time;

void Draw() {
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	
	game->draw();

	glFlush();
}

void Resize(int width, int height) {

	game->resize(width, height);
}
/*
void Update() {
	glutPostRedisplay();
}
*/
void Timer(int value) {

	const clock_t begin_time = clock();
	float timeDelta = (float)(begin_time - last_time) / CLOCKS_PER_SEC;
	//cout << "\ntime " << timeDelta;

	
	game->update(timeDelta);

	glutSwapBuffers();
	glutPostRedisplay();

	glutTimerFunc(1000 / 60, Timer, 0);

	last_time = begin_time;
}

void Keyboard(unsigned char key, int x, int y) {
	game->keyPressed(key);
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	/*screen = new MyScreen;
	screen->initialize(800, 600);*/


	game = new Game();
	game->initialize();

	glutDisplayFunc(Draw);
	glutReshapeFunc(Resize);
	/*
	//glutIdleFunc(Update);
	*/
	glutKeyboardFunc(Keyboard);
	glutTimerFunc(1000 / 60, Timer, 0);

	last_time = clock();
	glutMainLoop();

	return 0;
}