#include "game.h"

#include <GL/glut.h>


Game::Game()
{
	box = new Box(0, 0);
	box2 = new Box(-100, 0);
	box3 = new Box(200, 0);
	box4 = new Box(0, 150);
	box5 = new Box(0, -250);
}


Game::~Game()
{
}

void Game::initialize() {
	glutInitDisplayMode(0);
	glutInitWindowSize(800, 600);
	glutCreateWindow("OpenGLWindow");
}

void Game::resize(float w, float h) {
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-w / 2.0f, w / 2.0f, -h / 2.0f, h / 2.0f);
}

void Game::draw() {
	box->draw();
	box2->draw();
	box3->draw();
	box4->draw();
	box5->draw();
}

void Game::update(float timeDelta) {

	box2->setPosition(box2->getX() + 1, box2->getY());
	if (box->isInTheBox(box2->getX(), box2->getY()))
	{
		box2->setPosition(-100, 0);
	}

	box3->setPosition(box3->getX() - 1, box3->getY());
	if (box->isInTheBox(box3->getX(), box3->getY()))
	{
		box3->setPosition(150, 0);
	}

	box4->setPosition(box4->getX(), box4->getY() - 1);
	if (box->isInTheBox(box4->getX(), box4->getY()))
	{
		box4->setPosition(0, 200);
	}

	box5->setPosition(box5->getX(), box5->getY() + 1);
	if (box->isInTheBox(box5->getX(), box5->getY()))
	{
		box5->setPosition(0, -250);
	}

}

void Game::keyPressed(unsigned char key) {
	box->setPosition(box->getX() + 1, box->getY());
}