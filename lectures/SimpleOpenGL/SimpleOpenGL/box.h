#pragma once
class Box
{
public:
	Box(float x, float y);
	~Box();


	void draw();
	float getX();
	float getY();
	void setPosition(float x, float y);
	bool isInTheBox(float px, float py);

private:
	float x, y, width, height;
};

