#include <iostream>
#include <vector>
#include "Vector2.h"
#include "Circle.h"
#include "BoundingBox.h"

using namespace std;

void displayPolygon(vector<Vector2> polygon);

int main()
{
	vector<Vector2> polygon;
	for (int i = 0; i < 4; i++)
	{
		float x,y;
		cout << "Input x coordinate for a polygon" << endl;
		cin >> x;
		cout << "Input y coordinate for a polygon" << endl;
		cin >> y;
		polygon.push_back(Vector2(x, y));
	}
	
	BoundingBox boxA = BoundingBox(Vector2(polygon[0].getX(),polygon[0].getY()),Vector2(polygon[0].getX(),polygon[0].getY()));
	BoundingBox boxB = BoundingBox(Vector2(1, 0), Vector2(3,-4));

	for (int i = 0; i < polygon.size(); i++)
	{
		//First pair -Least X-
		if (polygon[i].getX() < boxA.getLeftX()) boxA.setLeftX(polygon[i].getX());
		//First pair -Greatest Y-
		if (polygon[i].getY() > boxA.getLeftY()) boxA.setLeftY(polygon[i].getY());
		//Second pair -Greatest X-
		if (polygon[i].getX() > boxA.getRightX()) boxA.setRightX(polygon[i].getX());
		//Second pair -Least Y-
		if (polygon[i].getY() < boxA.getRightY()) boxA.setRightY(polygon[i].getY());
	}

	cout << "Polygon A coordinates: " << endl;
	displayPolygon(polygon);

	cout << "Bounding Box A (from Polygon A) coordinates: " << endl;
	cout << "(" << boxA.getLeftX() << ", " << boxA.getLeftY() << ")" << endl;
	cout << "(" << boxA.getRightX() << ", " << boxA.getRightY() << ")" << endl;

	cout << "Bounding Box B coordinates: " << endl;
	cout << "(" << boxB.getLeftX() << ", " << boxB.getLeftY() << ")" << endl;
	cout << "(" << boxB.getRightX() << ", " << boxB.getRightY() << ")" << endl;

	boxA.checkCollission(boxB);
	system("pause");
	return 0;
}

void displayPolygon(vector<Vector2> polygon)
{
	for (int i = 0; i < polygon.size(); i++)
	{
		cout << "(" << polygon[i].getX() << ", " << polygon[i].getY() << ")" << endl;
	}
}