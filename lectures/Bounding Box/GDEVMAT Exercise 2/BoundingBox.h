#pragma once
#include "Vector2.h"
#include <iostream>

using namespace std;

class BoundingBox
{
public:
	BoundingBox();
	~BoundingBox();

	BoundingBox(Vector2 left, Vector2 right);

	float getLeftX();
	float getLeftY();
	float getRightX();
	float getRightY();

	void setLeftX(float x);
	void setLeftY(float y);
	void setRightX(float x);
	void setRightY(float y);

	void checkCollission(BoundingBox box);

private:
	Vector2 mLeft;
	Vector2 mRight;

};

