#include "BoundingBox.h"



BoundingBox::BoundingBox()
{
}


BoundingBox::~BoundingBox()
{
}

BoundingBox::BoundingBox(Vector2 left, Vector2 right)
{
	mLeft = left;
	mRight = right;
}

float BoundingBox::getLeftX()
{
	return mLeft.getX();
}

float BoundingBox::getLeftY()
{
	return mLeft.getY();
}

float BoundingBox::getRightX()
{
	return mRight.getX();
}

float BoundingBox::getRightY()
{
	return mRight.getY();
}

void BoundingBox::setLeftX(float x)
{
	mLeft.setX(x);
}

void BoundingBox::setLeftY(float y)
{
	mLeft.setY(y);
}

void BoundingBox::setRightX(float x)
{
	mRight.setX(x);
}

void BoundingBox::setRightY(float y)
{
	mRight.setY(y);
}

void BoundingBox::checkCollission(BoundingBox box)
{
	bool colliding = false;

	while(!colliding)
	{
		if (mRight.getX() > box.getLeftX())
		{
			if (mRight.getY() < box.getLeftY()) colliding = true;
		}

		if (mRight.getY() < box.getLeftY())
		{
			if (mRight.getX() > box.getLeftX()) colliding = true;
		}

		if (mLeft.getX() > box.getRightX())
		{
			if (mLeft.getY() < box.getRightY()) colliding = true;
		}

		if (mLeft.getY() < box.getRightY())
		{
			if (mLeft.getX() > box.getRightX()) colliding = true;
		}

		break;
	}

	if(colliding) cout << "The two boxes collide." << endl;
	else cout << "The two boxes do not collide." << endl;
}
