#include <iostream>
#include <vector>
#include "Vector2.h"
#include "Circle.h"

using namespace std;


int main()
{
	///Making a Vector2 Class
	//float aX, aY, bX, bY;

	//cout << "Input X for Vector A" << endl;
	//cin >> aX;
	//cout << "Input Y for Vector A" << endl;
	//cin >> aY;
	//Vector2 a = Vector2(aX, aY);

	//cout << "Input X for Vector B" << endl;
	//cin >> bX;
	//cout << "Input Y for Vector B" << endl;
	//cin >> bY;
	//Vector2 b = Vector2(bX, bY);

	//system("cls");
	//cout << "Vector A = " << "(" << a.getX() << ", " << a.getY() << ")" << endl;
	//cout << "Vector B = " << "(" << b.getX() << ", " << b.getY() << ")" << endl;
	//cout << "====================" << endl;

	//Vector2 c = a + b;
	//Vector2 d = a - b;
	//Vector2 e = a * b;
	//Vector2 f = a / b;
	//float aMagnitude = a.Magnitude();
	//float bMagnitude = b.Magnitude();

	//cout << "Addition: " << "(" << c.getX() << ", " << c.getY() << ")" << endl;
	//cout << "Subtraction: " << "(" << d.getX() << ", " << d.getY() << ")" << endl;
	//cout << "Multiplication: " << "(" << e.getX() << ", " << e.getY() << ")" << endl;
	//cout << "Division: " << "(" << f.getX() << ", " << f.getY() << ")" << endl;
	//cout << "Vector A Magnitude: " << aMagnitude << endl;
	//cout << "Vector B Magnitude: " << bMagnitude << endl;


	//Creating a box and rotating it
	//float length, width, centerX, centerY;
	//cout << "Input the X coordinate of your center" << endl;
	//cin >> centerX;
	//cout << "Input the Y coordinate of your center" << endl;
	//cin >> centerY;
	//cout << "Input length of the box" << endl;
	//cin >> length;
	//cout << "Input width of the box" << endl;
	//cin >> width;

	//length /= 2;
	//width /= 2;
	//vector <Vector2> box;
	//box.push_back(Vector2(centerX, centerY));
	//box.push_back(Vector2(centerX + length, centerY + width));
	//box.push_back(Vector2(centerX + length, centerY - width));
	//box.push_back(Vector2(centerX - length, centerY - width));
	//box.push_back(Vector2(centerX - length, centerY + width));

	//cout << "\nYour box's coordinates are: \nCenter: (" << box[0].getX() << ", " << box[0].getY() << ")\nCoordinate 1: (" << box[1].getX() << ", " << box[1].getY() << ")\nCoordinate 2: (" << box[2].getX() << ", " << box[2].getY() << ")\nCoordinate 3: (" << box[3].getX() << ", " << box[3].getY() << ")\nCoordinate 4: (" << box[4].getX() << ", " << box[4].getY() << ")" <<  endl;
	//
	//cout << "\nHow many degrees would you like to rotate your box?" << endl;
	//float degree;
	//float const converter = 3.14 / 180;
	//cin >> degree;
	//degree *= converter;

	//vector <Vector2> newbox;
	//newbox.push_back(Vector2(centerX, centerY));
	//newbox.push_back(Vector2((box[1].getX() * (cos(degree))) + (box[1].getY() * ( sin(degree))), (box[1].getX() * ( -sin(degree))) + (box[1].getY() * ( cos(degree)))));
	//newbox.push_back(Vector2((box[2].getX() * (cos(degree))) + (box[2].getY() * ( sin(degree))), (box[2].getX() * ( -sin(degree))) + (box[2].getY() * ( cos(degree)))));
	//newbox.push_back(Vector2((box[3].getX() * (cos(degree))) + (box[3].getY() * ( sin(degree))), (box[3].getX() * ( -sin(degree))) + (box[3].getY() * ( cos(degree)))));
	//newbox.push_back(Vector2((box[4].getX() * (cos(degree))) + (box[4].getY() * ( sin(degree))), (box[4].getX() * ( -sin(degree))) + (box[4].getY() * ( cos(degree)))));

	//cout << "\nYour rotated box's coordinates are: \nCenter: (" << newbox[0].getX() << ", " << newbox[0].getY() << ")\nCoordinate 1: (" << newbox[1].getX() << ", " << newbox[1].getY() << ")\nCoordinate 2: (" << newbox[2].getX() << ", " << newbox[2].getY() << ")\nCoordinate 3: (" << newbox[3].getX() << ", " << newbox[3].getY() << ")\nCoordinate 4: (" << newbox[4].getX() << ", " << newbox[4].getY() << ")\n" << endl;


	//Checking for collision between 2 circles

	float aX, aY, aR, bX, bY, bR;

	cout << "Please input Circle A's center X coordinate:" << endl;
	cin >> aX;
	cout << "Please input Circle A's center Y coordinate:" << endl;
	cin >> aY;
	cout << "Please input Circle A's radius:" << endl;
	cin >> aR;

	cout << "Please input Circle B's center X coordinate:" << endl;
	cin >> bX;
	cout << "Please input Circle B's center Y coordinate:" << endl;
	cin >> bY;
	cout << "Please input Circle B's radius:" << endl;
	cin >> bR;
	system("cls");

	Circle a = Circle(Vector2(aX, aY), aR);
	Circle b = Circle(Vector2(bX, bY), bR);
	float distance = a.getDistanceFrom(b);
	float totalRadius = a.getRadius() + b.getRadius();

	cout << "Circle A \nCenter: (" << a.getX() << ", " << a.getY() << ")\nRadius: " << a.getRadius() << endl;
	cout << "\nCircle B \nCenter: (" << b.getX() << ", " << b.getY() << ")\nRadius: " << b.getRadius() << endl;
	cout << "\nDistance between Circle A and B: " << distance << endl;
	cout << "Total radius of Circle A and B:  " << totalRadius << endl;
	cout << "\nCircle B will now move to Circle A." << endl;
	system("pause");

	system("cls");

	Vector2 direction = Vector2(b.getX() - a.getX(), b.getY() - a.getY());
	float magnitude = sqrt((direction.getX() * direction.getX()) + (direction.getY() * direction.getY()));
	Vector2 normalized = Vector2(direction.getX() / magnitude, direction.getY() / magnitude);

	while (true)
	{
		if (distance < totalRadius)
		{
			cout << "\nCircle B has collided with Circle A." << endl;
			break;
		}

		b.setPosition(b.getX() - normalized.getX(), b.getY() - normalized.getY());
		distance = a.getDistanceFrom(b);
		cout << "\nDistance between Circle A and B: " << distance << endl;
		cout << "Total radius of Circle A and B:  " << totalRadius << endl;
		system("pause");
	}


	system("pause");
	return 0;
}