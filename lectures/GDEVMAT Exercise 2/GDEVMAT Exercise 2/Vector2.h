#pragma once
#include <math.h>

class Vector2
{
public:
	Vector2(float x, float y);
	Vector2();
	~Vector2();

	float getX();
	float getY();

	Vector2 operator+(Vector2 vector);
	Vector2 operator-(Vector2 vector);
	Vector2 operator*(Vector2 vector);
	Vector2 operator/(Vector2 vector);

	float Magnitude();

private:
	float mX;
	float mY;
};

